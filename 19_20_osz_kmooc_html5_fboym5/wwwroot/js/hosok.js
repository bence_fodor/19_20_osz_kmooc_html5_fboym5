﻿function hosLeker(start, count) {
    var adat = null;
    var xmlReq = new XMLHttpRequest();

    xmlReq.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            $('#loadingRow').remove();
            if (this.status === 200) {
                console.log(this.responseText);
                hosFeldolg(this.responseText);
            }
            else {
                $('#table').append('<tr><td>Szerver nem valaszol</td><td>Szerver nem valaszol</td><td>Szerver nem valaszol</td><td>Szerver nem valaszol</td><td>Szerver nem valaszol</td></tr>');
            }
        }
    });

    xmlReq.open("GET", "http://81.2.241.234:8080/hero?start=" + start + "& count=" + count + "& orderfield=name & orderdirection=ASC");


    xmlReq.send(adat);
}

function hosFeldolg(json) {
    var lista = JSON.parse(json);
    var lenghth = Object.keys(lista).length;
    utolsoFogadott = lenghth;

    if (lenghth > 0) {
        for (var i = 0; i < lenghth; i++) {
            $('#table').append('<tr><td>' + lista[i].id + '</td><td>' + lista[i].name + '</td><td>' + lista[i].description + '</td><td><a href="#" onclick="modosit(' + lista[i].id + ');">Modosit</a></td><td><a href="#" onclick="torol(' + lista[i].id + ');">Torol</a></td></tr>');
        }
    }
}

function hosKeresId(id) {
    var adat = null;
    var xmlReq = new XMLHttpRequest();

    xmlReq.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                console.log(this.responseText);

                $('#loading').remove();
                $('#modifyButton').prop('disabled', false);
                $('#Id').prop('disabled', true);
                $('#name').prop('disabled', false);
                $('#desc').prop('disabled', false);

                $('#modifyButton').on("click", function () {
                    var loading = "<img id='loading' src= 'https://loading.io/spinners/curve-bars/lg.curved-bar-spinner.gif' style='width:50px; height:50px;'/>";

                    hosModosit($('#Id').val(), $('#name').val(), $('#desc').val());
                    document.getElementById("mp").innerHTML = "<h2>Saving modifications in progress...</h2><br>" + loading;
                });

                var json = JSON.parse(this.responseText);
                $('#Id').val(json.id);
                $('#name').val(json.name);
                $('#desc').val(json.description);



            }
            else {
                alert("Adott azonosito adatai nem tolthetok be. Szerver nem valaszol.");
                $('#loading').remove();
                $('#Id').val("Szerver nem valaszol");
                $('#name').val("Szerver nem valaszol");
                $('#desc').val("Szerver nem valaszol");
                $('#modifyButton').prop('disabled', true);
            }
        }
    });

    xmlReq.open("GET", "http://81.2.241.234:8080/hero/" + id);

    xmlReq.send(adat);
}

function hosModosit(idYouWantToModify, newName, newDesc) {
    var adat = new FormData();
    var xmlReq = new XMLHttpRequest();

    adat.append("name", newName);
    adat.append("desc", newDesc);

    xmlReq.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                console.log(this.responseText);
                $("#mp").html("<h2>Sikeres muvelet, kattintson <a href='#' onclick='ujrakezd();'>ide</a> amennyiben vissza akar terni. </h2>");

            }
            else {
                $("#mp").html("<h2>Hiba: Szerver nem valaszol. Probalja meg kesobb. Kattintson <a href='#' onclick='ujrakezd();'>ide</a> amennyiben vissza akar terni. </h2>");
            }
        }
    });

    xmlReq.open("PUT", "http://81.2.241.234:8080/hero/" + idYouWantToModify);


    xmlReq.send(adat);
}

function hosTorolId(id) {
    var adat = null;
    var xmlReq = new XMLHttpRequest();

    xmlReq.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            $('#loadingRow').remove();
            if (this.status === 200) {
                console.log(this.responseText);
                alert(id + " succesfully deleted!");
                ujraStart();
            }
            else {
                alert(id + " torlese sikertelen, probalja meg kesobb.");
            }
        }
    });

    xmlReq.open("DELETE", "http://81.2.241.234:8080/hero/" + id);


    xmlReq.send(adat);
}

function hosHozzaad(name, desc) {
    var adat = new FormData();
    var xmlReq = new XMLHttpRequest();

    adat.append("name", name);
    adat.append("desc", desc);

    xmlReq.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                console.log(this.responseText);
                $("#mp").html("<h2>Uj hos sikeresen letrehozva. Kattintson <a href='#' onclick='ujrakezd();'>ide</a> amennyiben vissza akar terni. </h2>");
            }
            else {
                $("#mp").html("<h2>Hiba, probalja meg kesobb. Kattinston <a href='#' onclick='ujrakezd();'>ide</a> amennyiben vissza akar terni. </h2>");

            }
        }
    });

    xmlReq.open("POST", "http://81.2.241.234:8080/hero");


    xmlReq.send(adat);
}

function modosit(id) {
    var loading = "<img id='loading' src= 'https://loading.io/spinners/curve-bars/lg.curved-bar-spinner.gif' style='width:50px; height:50px;'/>";
    var m = '<div class="container">';
    m += '<h2>Modosit</h2>';
    m += loading;

    m += '<div class="form-group"><label for="Id">Id:</label><input type="text" class="form-control" id="Id" placeholder="Betoltes folyamatban..." name="Id" disabled></div>';
    m += '<div class="form-group"><label for="name">Nev: </label><input type="text" class="form-control" id="name" placeholder="Betoltes folyamatban..." name="name"></div><div class="form-group">';
    m += '<label for="desc">Leiras:</label><input type="text" class="form-control" id="desc" placeholder="Betoltes folyamatban..." name="desc"></div><button id="modifyButton"  class="btn btn-primary">Modosit</button>';

    m += '      <a href="#" onclick="ujrakezd();">Visszateres az osszes hos nezethez</a> </div>';
    document.getElementById("mp").innerHTML = m;
    $('#modifyButton').prop('disabled', true);
    $('#Id').prop('disabled', true);
    $('#name').prop('disabled', true);
    $('#desc').prop('disabled', true);
    hosKeresId(id);
}

function torol(id) {
    var loading = "<img src= 'https://loading.io/spinners/curve-bars/lg.curved-bar-spinner.gif' style='width:50px; height:50px;'/>";

    var m = "<tr id='loadingRow'><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td></tr>";
    $('#table').append(m);
    hosTorolId(id);
}

function ujHozzaad() {
    var loading = "<img id='loading' src= 'https://loading.io/spinners/curve-bars/lg.curved-bar-spinner.gif' style='width:50px; height:50px;'/>";
    var m = '<div class="container">';
    m += '<h2>Uj faj felvetele</h2>';
    m += loading;
    m += '<div class="form-group"><label for="name">Nev: </label><input type="text" class="form-control" id="name" placeholder="Adja meg a nevet" name="name"></div><div class="form-group">';
    m += '<label for="desc">Leiras:</label><input type="text" class="form-control" id="desc" placeholder="Adja meg a leirast" name="desc"></div><button id="modifyButton"  class="btn btn-primary">Uj felvetele</button>';
    m += '      <a href="#" onclick="ujrakezd();">Visszateres az osszes hos nezethez</a> </div>';
    document.getElementById("mp").innerHTML = m;
    $("#loading").prop("hidden", true);
    $("#modifyButton").on("click", function () {
        $("#loading").prop("hidden", false);
        $("#name").prop("disabled", true);
        $("#desc").prop("disabled", true);
        $("#modifyButton").prop("disabled", true);
        hosHozzaad($("#name").val(), $("#desc").val());
    });
}

var kezdoSor = 0;
var sorok = 10;
var utolsoFogadott = 0;

function ujraStart() {

    var mp = document.getElementById("mp");

    var m = '<h2>Hosok listaja</h2><a href="#" onclick="ujHozzaad();">Uj hos felvetele</a><table id="table" name="table" class="table table-dark table-hover"><thead><tr><th>Id</th><th>Nev</th><th>Leiras</th><th>Modosit</th><th>Torol</th></tr></thead><tbody>';
    var loading = "<img src= 'https://loading.io/spinners/curve-bars/lg.curved-bar-spinner.gif' style='width:50px; height:50px;'/>";

    m += "<tr id ='loadingRow'><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td></tr>";
    m += "</tbody></table>";
    m += '<a style="float:left; background-color:black;" href="#" onclick="elozo();" id="prev">Elozo 10 sor</a>';
    m += '<a style="float:right; background-color:black;" href="#" onclick="kovetkezo();" id="next">Kovetkezo 10 sor</a>';

    mp.innerHTML = m;

    hosLeker(kezdoSor, sorok);
}

function elozo() {
    if (kezdoSor !== 0) {
        kezdoSor -= 10;

        ujraStart();
    }
}

function kovetkezo() {
    if (utolsoFogadott === sorok) {
        kezdoSor += 10;
        ujraStart();
    }
}

$('document').ready(function () {
    function start() {
        var mp = document.getElementById("mp");

        var m = '<h2>Hosok listaja</h2><a href="#" onclick="ujHozzaad();">Uj hos felvetele</a><table id="table" name="table" class="table table-dark table-hover"><thead><tr><th>Id</th><th>Nev</th><th>Leiras</th><th>Modosit</th><th>Torol</th></tr></thead><tbody>';
        var loading = "<img id='loading' src= 'https://loading.io/spinners/curve-bars/lg.curved-bar-spinner.gif' style='width:50px; height:50px;'/>";

        m += "<tr id='loadingRow'><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td><td>" + loading + "</td></tr>";
        m += "</tbody></table>";
        m += '<a style="float:left; background-color:black;" href="#" onclick="elozo();" id="prev">Elozo 10 sor</a>';
        m += '<a style="float:right; background-color:black;" href="#" onclick="kovetkezo();" id="next">Kovetkezo 10 sor</a>';

        mp.innerHTML = m;

        hosLeker(kezdoSor, sorok);
    }

    start();
});



